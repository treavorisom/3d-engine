/* -------- Bitmap.h -------- */ #pragma once

#include "Syntax.h"

#include <fstream>

#pragma pack(1) 
struct BitmapFileHeader {
	char id[2];
	signed long size;
	signed long reserved;
	signed long arrayOffset;
};

#pragma pack(1)
struct BitmapCoreHeader {
	unsigned long headerSize;
	unsigned short width;
	unsigned short height;
	unsigned short numColorPlanes;
	unsigned short bitsPerPixel;
};

#pragma pack(1)
struct BitmapInfoHeader {
	unsigned long headerSize;
	signed long width;
	signed long height;
	unsigned short numColorPlanes;
	unsigned short bitsPerPixel;

	unsigned long compressionMethod;
	unsigned long imageSize;
	unsigned long horizontalResolution;
	unsigned long verticalResolution;
	unsigned long numColors;
	unsigned long numImportantColors;
};

class Bitmap {
	Private unsigned int width, height;
	Private unsigned int bitsPerPixel;
	Private unsigned int dataSize;
	Private byte* data;

	Public Bitmap() {}
	Public Bitmap(const char* fileName) { initialize(fileName); }
	Public ~Bitmap() { release(); }

	Public int initialize(const char* fileName);
	Public void release() { if (data) delete[] data; data = NULL; }

	Public unsigned int getWidth() { return width; }
	Public unsigned int getHeight() { return height; }
	Public unsigned int getBitsPerPixel() { return bitsPerPixel; }
	Public unsigned int getDataSize() { return dataSize; }
	Public byte* getData() { return data; }

	Private BitmapFileHeader fileHeader;
};

int Bitmap::initialize(const char* fileName) {

	// Open bitmap file
	std::ifstream file(fileName, std::ios::in | std::ios::binary | std::ios::ate);

	unsigned int fileSize = 0;

	if (file.is_open()) {

		// Get the file size
		fileSize = file.tellg();
		file.seekg(0, std::ios::beg);

		// Get the file header
		file.read((char*)&fileHeader, sizeof(BitmapFileHeader));

		unsigned int infoHeraderSize;
		file.seekg(sizeof(BitmapFileHeader), std::ios::beg);
		file.read((char*)&infoHeraderSize, 4);

		switch (infoHeraderSize) {
			case sizeof(BitmapCoreHeader) :
			{
				BitmapCoreHeader infoHeader;
				file.seekg(sizeof(BitmapFileHeader), std::ios::beg);
				file.read((char*)&infoHeader, sizeof(BitmapCoreHeader));

				width = infoHeader.width;
				height = infoHeader.height;
				bitsPerPixel = infoHeader.bitsPerPixel;

				//if (infoHeader.bitsPerPixel != 32){ file.close(); return false; }     // Error

				dataSize = width * height * 4;
				data = new unsigned char[dataSize];

				file.seekg(fileHeader.arrayOffset, std::ios::beg);
				file.read((char*)data, dataSize);

				break;
			}
			case sizeof(BitmapInfoHeader) :
			case 52  : // BITMAPV2INFOHEADER
			case 56  : // BITMAPV3INFOHEADER
			case 108 : // BITMAPV4HEADER
			case 124 : // BITMAPV5HEADER
			{
				BitmapInfoHeader infoHeader;
				file.seekg(sizeof(BitmapFileHeader), std::ios::beg);
				file.read((char*)&infoHeader, sizeof(BitmapInfoHeader));

				width = infoHeader.width;
				height = infoHeader.height;
				bitsPerPixel = infoHeader.bitsPerPixel;

				//if (infoHeader.compressionMethod != 0){ file.close(); return false; } // Error
				//if (infoHeader.bitsPerPixel != 32){ file.close(); return false; }     // Error

				dataSize = width * height * 4;
				data = new unsigned char[dataSize];

				file.seekg(fileHeader.arrayOffset, std::ios::beg);
				file.read((char*)data, dataSize);

				for (int i = 0; i < height; i++) {
					int row = i * width * 4;
					for (int j = 0; j < width; j++) {
						unsigned char x, r, g, b, a;

							// ----- WHY is it backwards?
							
						x = data[row + j*4 + 0];
						r = data[row + j*4 + 1];
						g = data[row + j*4 + 2];
						b = data[row + j*4 + 3];

						data[row + j*4 + 0] = b;
						data[row + j*4 + 1] = g;
						data[row + j*4 + 2] = r;
						data[row + j*4 + 3] = x;
					}
				}

				break;
			}
			default:
			{
				// Error
				file.close();
				return false;
			}
		}

		file.close();
	}
	else
		return false;

	return true;
}