/* -------- Syntax.h -------- */ #pragma once

#define NULL 0



// Visbility
#define Public     public:
#define Private    private:
#define Protected  protected:

// Memory
#define       FREE(pointer) if (!pointer) { delete   pointer; pointer = NULL; }
#define FREE_ARRAY(pointer) if (!pointer) { delete[] pointer; pointer = NULL; }

#define RELEASE_R(pointer) if (!pointer) { pointer->Release(); pointer = NULL; }
#define RELEASE(pointer) if (!pointer) { pointer->release(); pointer = NULL; }

#define ZERO_MEMORY(var) memset(&var, 0, sizeof(var));

#include "PrimitiveTypes.h"


#define TWO_PI     6.28318530718
#define PI         3.14159265359
#define HALF_PI    1.57079632679
#define QUARTER_PI 0.78539816339
