/* |-------|    DirectX.h    |-------| */ #pragma once

#pragma comment(lib, "dxgi.lib")
#pragma comment(lib, "d3d11.lib")
//#pragma comment(lib, "d3dx11.lib")
//#pragma comment(lib, "d3dx10.lib")

#include <dxgi.h>
#include <d3dcommon.h>
#include <d3d11.h>

#include "Syntax.h"


#include "VideoCard.h"
//#include "ViewMatrix.h"
//#include "ColorShader.h"

class Direct3DDevice {

	Public struct Direct3DSettings {
		BOOL vSyncEnabled;
		BOOL fullscreenEnabled;

		float screenDepth;
		float screenNear;
	};

	// Fields
	Private ID3D11Device*        device;
	Private ID3D11DeviceContext* deviceContext;

	Private IDXGISwapChain*         swapChain;
	Private ID3D11RenderTargetView* renderTargetView;
	Private ID3D11RasterizerState*  rasterState;
	
	Private ID3D11Texture2D*        depthStencilBuffer;
	Private ID3D11DepthStencilView* depthStencilView;

	// States
	Private ID3D11DepthStencilState* depthStencilState;
	Private ID3D11DepthStencilState* depthDisabledStencilState;

	Private ID3D11BlendState* alphaEnableBlendingState;
	Private ID3D11BlendState* alphaDisableBlendingState;

	Public Direct3DSettings settings;

	// Methods
	Public HRESULT initialize(HWND hwnd);
	Public void release();

	Public void beginScene();
	Public void endScene();

	Public void enableAlphaBlending();
	Public void disableAlphaBlending();

	Public void enableZBuffer();
	Public void disableZBuffer();

	Public ID3D11Device* getDevice() { return device; }
	Public ID3D11DeviceContext* getDeviceContext() { return deviceContext; }


	Private void createDepthBuffer();
};

void Direct3DDevice::release() {
	// Before shutting down set to windowed mode or when you release the swap chain it will throw an exception
	if (!swapChain) swapChain->SetFullscreenState(false, NULL);

	RELEASE_R(alphaEnableBlendingState);
	RELEASE_R(alphaDisableBlendingState);
	RELEASE_R(depthDisabledStencilState);
	RELEASE_R(rasterState);
	RELEASE_R(depthStencilView);
	RELEASE_R(depthStencilState);
	RELEASE_R(depthStencilBuffer);
	RELEASE_R(renderTargetView);
	RELEASE_R(deviceContext);
	RELEASE_R(device);
	RELEASE_R(swapChain);
}

//void Direct3DDevice::createDepthBuffer(int width, int height) {
//
//
//	ID3D11Texture2D* pDepthStencil = NULL;
//	D3D11_TEXTURE2D_DESC descDepth;
//	descDepth.Width = width;
//	descDepth.Height = height;
//	descDepth.MipLevels = 1;
//	descDepth.ArraySize = 1;
//	descDepth.Format = pDeviceSettings->d3d11.AutoDepthStencilFormat;
//	descDepth.SampleDesc.Count = 1;
//	descDepth.SampleDesc.Quality = 0;
//	descDepth.Usage = D3D11_USAGE_DEFAULT;
//	descDepth.BindFlags = D3D11_BIND_DEPTH_STENCIL;
//	descDepth.CPUAccessFlags = 0;
//	descDepth.MiscFlags = 0;
//	hr = pd3dDevice->CreateTexture2D(&descDepth, NULL, &pDepthStencil);
//
//
//
//
//
//	D3D11_DEPTH_STENCIL_DESC dsDesc;
//
//	// Depth test parameters
//	dsDesc.DepthEnable = true;
//	dsDesc.DepthWriteMask = D3D11_DEPTH_WRITE_MASK_ALL;
//	dsDesc.DepthFunc = D3D11_COMPARISON_LESS;
//
//	// Stencil test parameters
//	dsDesc.StencilEnable = true;
//	dsDesc.StencilReadMask = 0xFF;
//	dsDesc.StencilWriteMask = 0xFF;
//
//	// Stencil operations if pixel is front-facing
//	dsDesc.FrontFace.StencilFailOp = D3D11_STENCIL_OP_KEEP;
//	dsDesc.FrontFace.StencilDepthFailOp = D3D11_STENCIL_OP_INCR;
//	dsDesc.FrontFace.StencilPassOp = D3D11_STENCIL_OP_KEEP;
//	dsDesc.FrontFace.StencilFunc = D3D11_COMPARISON_ALWAYS;
//
//	// Stencil operations if pixel is back-facing
//	dsDesc.BackFace.StencilFailOp = D3D11_STENCIL_OP_KEEP;
//	dsDesc.BackFace.StencilDepthFailOp = D3D11_STENCIL_OP_DECR;
//	dsDesc.BackFace.StencilPassOp = D3D11_STENCIL_OP_KEEP;
//	dsDesc.BackFace.StencilFunc = D3D11_COMPARISON_ALWAYS;
//
//	// Create depth stencil state
//	ID3D11DepthStencilState * pDSState;
//	pd3dDeviceContext->CreateDepthStencilState(&dsDesc, &pDSState);
//
//
//
//	// Bind depth stencil state
//	pDevice->OMSetDepthStencilState(pDSState, 1);
//
//
//
//	D3D11_DEPTH_STENCIL_VIEW_DESC descDSV;
//	descDSV.Format = DXGI_FORMAT_D32_FLOAT_S8X24_UINT;
//	descDSV.ViewDimension = D3D11_DSV_DIMENSION_TEXTURE2D;
//	descDSV.Texture2D.MipSlice = 0;
//
//	// Create the depth stencil view
//	ID3D11DepthStencilView* pDSV;
//	hr = pd3dDevice->CreateDepthStencilView(pDepthStencil, // Depth stencil texture
//		&descDSV, // Depth stencil desc
//		&pDSV);  // [out] Depth stencil view
//
//	// Bind the depth stencil view
//	pd3dDeviceContext->OMSetRenderTargets(1,          // One rendertarget view
//		&pRTV,      // Render target view, created earlier
//		pDSV);     // Depth stencil view for the render target
//
//
//
//
//
//
//
//
//}


HRESULT Direct3DDevice::initialize(HWND hwnd) {

	RECT rc;
	GetClientRect(hwnd, &rc);
	UINT clientWidth = rc.right - rc.left;
	UINT clientHeight = rc.bottom - rc.top;

	DXGI_SWAP_CHAIN_DESC swapChainDesc;
	ZeroMemory(&swapChainDesc, sizeof(DXGI_SWAP_CHAIN_DESC));

	swapChainDesc.BufferCount = 1;
	swapChainDesc.BufferDesc.Width = clientWidth;
	swapChainDesc.BufferDesc.Height = clientHeight;
	swapChainDesc.BufferDesc.Format = DXGI_FORMAT_R8G8B8A8_UNORM;
	swapChainDesc.BufferDesc.RefreshRate.Numerator = 0;
	swapChainDesc.BufferDesc.RefreshRate.Denominator = 1;
	swapChainDesc.BufferUsage = DXGI_USAGE_RENDER_TARGET_OUTPUT;
	swapChainDesc.OutputWindow = hwnd;
	swapChainDesc.SampleDesc.Count = 1;
	swapChainDesc.SampleDesc.Quality = 0;
	swapChainDesc.SwapEffect = DXGI_SWAP_EFFECT_DISCARD;
	swapChainDesc.Windowed = TRUE;

	UINT createDeviceFlags = 0;
#if _DEBUG
	createDeviceFlags |= D3D11_CREATE_DEVICE_DEBUG;
#endif

	// These are the feature levels that we will accept.
	D3D_FEATURE_LEVEL featureLevels[] =
	{
		D3D_FEATURE_LEVEL_11_0,
		D3D_FEATURE_LEVEL_10_1,
		D3D_FEATURE_LEVEL_10_0,
		D3D_FEATURE_LEVEL_9_3,
		D3D_FEATURE_LEVEL_9_2,
		D3D_FEATURE_LEVEL_9_1
	};

	// This will be the feature level that 
	// is used to create our device and swap chain.
	D3D_FEATURE_LEVEL featureLevel;

	HRESULT hr = D3D11CreateDeviceAndSwapChain(nullptr, D3D_DRIVER_TYPE_HARDWARE,
		nullptr, createDeviceFlags, featureLevels, _countof(featureLevels),
		D3D11_SDK_VERSION, &swapChainDesc, &swapChain, &device, &featureLevel,
		&deviceContext);

	// Next initialize the back buffer of the swap chain and associate it to a 
	// render target view.
	ID3D11Texture2D* backBuffer;
	hr = swapChain->GetBuffer(0, __uuidof(ID3D11Texture2D), (LPVOID*)&backBuffer);
	if (FAILED(hr))
	{
		return -1;
	}

	hr = device->CreateRenderTargetView(backBuffer, nullptr, &renderTargetView);
	if (FAILED(hr))
	{
		return -1;
	}

	backBuffer->Release();



	// Create the depth buffer for use with the depth/stencil view.
	D3D11_TEXTURE2D_DESC depthStencilBufferDesc;
	ZeroMemory(&depthStencilBufferDesc, sizeof(D3D11_TEXTURE2D_DESC));

	depthStencilBufferDesc.ArraySize = 1;
	depthStencilBufferDesc.BindFlags = D3D11_BIND_DEPTH_STENCIL;
	depthStencilBufferDesc.CPUAccessFlags = 0; // No CPU access required.
	depthStencilBufferDesc.Format = DXGI_FORMAT_D24_UNORM_S8_UINT;
	depthStencilBufferDesc.Width = clientWidth;
	depthStencilBufferDesc.Height = clientHeight;
	depthStencilBufferDesc.MipLevels = 1;
	depthStencilBufferDesc.SampleDesc.Count = 1;
	depthStencilBufferDesc.SampleDesc.Quality = 0;
	depthStencilBufferDesc.Usage = D3D11_USAGE_DEFAULT;

	hr = device->CreateTexture2D(&depthStencilBufferDesc, nullptr, &depthStencilBuffer);
	if (FAILED(hr))
	{
		return -1;
	}


	hr = device->CreateDepthStencilView(depthStencilBuffer, nullptr, &depthStencilView);
	if (FAILED(hr))
	{
		return -1;
	}


	// Setup depth/stencil state.
	D3D11_DEPTH_STENCIL_DESC depthStencilStateDesc;
	ZeroMemory(&depthStencilStateDesc, sizeof(D3D11_DEPTH_STENCIL_DESC));

	depthStencilStateDesc.DepthEnable = TRUE;
	depthStencilStateDesc.DepthWriteMask = D3D11_DEPTH_WRITE_MASK_ALL;
	depthStencilStateDesc.DepthFunc = D3D11_COMPARISON_LESS;
	depthStencilStateDesc.StencilEnable = FALSE;

	hr = device->CreateDepthStencilState(&depthStencilStateDesc, &depthStencilState);


	// Setup rasterizer state.
	D3D11_RASTERIZER_DESC rasterizerDesc;
	ZeroMemory(&rasterizerDesc, sizeof(D3D11_RASTERIZER_DESC));

	rasterizerDesc.AntialiasedLineEnable = FALSE;
	rasterizerDesc.CullMode = D3D11_CULL_BACK;
	rasterizerDesc.DepthBias = 0;
	rasterizerDesc.DepthBiasClamp = 0.0f;
	rasterizerDesc.DepthClipEnable = TRUE;
	rasterizerDesc.FillMode = D3D11_FILL_SOLID;
	rasterizerDesc.FrontCounterClockwise = FALSE;
	rasterizerDesc.MultisampleEnable = FALSE;
	rasterizerDesc.ScissorEnable = FALSE;
	rasterizerDesc.SlopeScaledDepthBias = 0.0f;

	// Create the rasterizer state object.
	hr = device->CreateRasterizerState(&rasterizerDesc, &rasterState);
	if (FAILED(hr))
	{
		return -1;
	}


	D3D11_VIEWPORT viewport;

	// Initialize the viewport to occupy the entire client area.
	viewport.Width = static_cast<float>(clientWidth);
	viewport.Height = static_cast<float>(clientHeight);
	viewport.TopLeftX = 0.0f;
	viewport.TopLeftY = 0.0f;
	viewport.MinDepth = 0.0f;
	viewport.MaxDepth = 1.0f;



	deviceContext->RSSetState(rasterState);
	deviceContext->RSSetViewports(1, &viewport);

	deviceContext->OMSetRenderTargets(1, &renderTargetView, depthStencilView);
	deviceContext->OMSetDepthStencilState(depthStencilState, 1);

	return 0;
}


//void Direct3DDevice::initialize(HWND hwnd) {
//
//	VideoCard videoCard;
//
//	//videoCard.
//
//
//	//unsigned int screenWidth = Windows::getScreenWidth();
//	//unsigned int screenHeight = Windows::getScreenHeight();
//
//	RECT clientRect;
//	GetClientRect(hwnd, &clientRect);
//
//	unsigned int screenWidth = clientRect.right - clientRect.left;
//	unsigned int screenHeight = clientRect.bottom - clientRect.top;
//
//	// Initialize the swap chain description
//	DXGI_SWAP_CHAIN_DESC swapChainDesc;
//	ZERO_MEMORY(swapChainDesc);
//
//	swapChainDesc.BufferCount       = 1; // Set to a single back buffer
//	swapChainDesc.BufferDesc.Width  = screenWidth;
//	swapChainDesc.BufferDesc.Height = screenHeight;
//	swapChainDesc.BufferDesc.Format = DXGI_FORMAT_R8G8B8A8_UNORM;// Set regular 32-bit surface for the back buffer
//	//swapChainDesc.BufferDesc.RefreshRate.Numerator   = settings.vSyncEnabled ? refreshRate.Numerator   : 0;
//	//swapChainDesc.BufferDesc.RefreshRate.Denominator = settings.vSyncEnabled ? refreshRate.Denominator : 1;
//
//	swapChainDesc.BufferDesc.RefreshRate.Numerator   = 60;
//	swapChainDesc.BufferDesc.RefreshRate.Denominator = 1;
//
//	swapChainDesc.BufferUsage                 = DXGI_USAGE_RENDER_TARGET_OUTPUT;
//	swapChainDesc.OutputWindow                = hwnd;
//	swapChainDesc.SampleDesc.Count            = 1;
//	swapChainDesc.SampleDesc.Quality          = 0;
//	swapChainDesc.Windowed                    = !settings.fullscreenEnabled;
//	swapChainDesc.BufferDesc.ScanlineOrdering = DXGI_MODE_SCANLINE_ORDER_UNSPECIFIED;
//	swapChainDesc.BufferDesc.Scaling          = DXGI_MODE_SCALING_UNSPECIFIED;
//	swapChainDesc.SwapEffect                  = DXGI_SWAP_EFFECT_DISCARD;
//	swapChainDesc.Flags                       = 0;
//
//	// Set the feature level to DirectX 11
//	D3D_FEATURE_LEVEL featureLevel = D3D_FEATURE_LEVEL_11_0;
//
//	// Create the swap chain, Direct3D device, and Direct3D device context
//	D3D11CreateDeviceAndSwapChain(NULL, D3D_DRIVER_TYPE_HARDWARE, NULL, D3D11_CREATE_DEVICE_DEBUG, &featureLevel, 1, D3D11_SDK_VERSION, &swapChainDesc, &swapChain, &device, NULL, &deviceContext);
//
//	// Get the pointer to the back buffer
//	ID3D11Texture2D* backBufferPtr;
//	swapChain->GetBuffer(0, __uuidof(ID3D11Texture2D), (LPVOID*)&backBufferPtr);
//
//	// Create the render target view with the back buffer pointer
//	device->CreateRenderTargetView(backBufferPtr, NULL, &renderTargetView);
//
//	// Release pointer to the back buffer as we no longer need it
//	backBufferPtr->Release();
//	backBufferPtr = 0;
//
//	// Initialize the description of the depth buffer
//	D3D11_TEXTURE2D_DESC depthBufferDesc;
//	ZERO_MEMORY(depthBufferDesc);
//
//	depthBufferDesc.Width = screenWidth;
//	depthBufferDesc.Height = screenHeight;
//	depthBufferDesc.MipLevels          = 1;
//	depthBufferDesc.ArraySize          = 1;
//	depthBufferDesc.Format = DXGI_FORMAT_D24_UNORM_S8_UINT;
//	depthBufferDesc.SampleDesc.Count   = 1;
//	depthBufferDesc.SampleDesc.Quality = 0;
//	depthBufferDesc.Usage              = D3D11_USAGE_DEFAULT;
//	depthBufferDesc.BindFlags          = D3D11_BIND_DEPTH_STENCIL;
//	depthBufferDesc.CPUAccessFlags     = 0;
//	depthBufferDesc.MiscFlags          = 0;
//
//	// Create the texture for the depth buffer using the filled out description
//	if (FAILED(device->CreateTexture2D(&depthBufferDesc, NULL, &depthStencilBuffer)))
//		std::cout << "Failed CreateTexture2D \n";
//
//	// Initialize the depth stencil view
//	D3D11_DEPTH_STENCIL_VIEW_DESC depthStencilViewDesc;
//	ZERO_MEMORY(depthStencilViewDesc);
//	depthStencilViewDesc.Format = DXGI_FORMAT_D24_UNORM_S8_UINT;
//	depthStencilViewDesc.ViewDimension      = D3D11_DSV_DIMENSION_TEXTURE2D;
//	depthStencilViewDesc.Texture2D.MipSlice = 0;
//
//	// Create the depth stencil view
//	if (FAILED(device->CreateDepthStencilView(depthStencilBuffer, &depthStencilViewDesc, &depthStencilView)))
//		std::cout << "Failed CreateDepthStencilView \n";
//
//	// Bind the render target view and depth stencil buffer to the output render pipeline
//	deviceContext->OMSetRenderTargets(1, &renderTargetView, depthStencilView);
//
//	//// Setup the raster description which will determine how and what polygons will be drawn
//	//D3D11_RASTERIZER_DESC rasterDesc;
//	//rasterDesc.AntialiasedLineEnable = false;
//	//rasterDesc.CullMode              = D3D11_CULL_BACK;
//	//rasterDesc.DepthBias             = 0;
//	//rasterDesc.DepthBiasClamp        = 0.0f;
//	//rasterDesc.DepthClipEnable       = false;// true
//	//rasterDesc.FillMode              = D3D11_FILL_SOLID;  //D3D11_FILL_SOLID;
//	//rasterDesc.FrontCounterClockwise = true;
//	//rasterDesc.MultisampleEnable     = false;
//	//rasterDesc.ScissorEnable         = false;
//	//rasterDesc.SlopeScaledDepthBias  = 0.0f;
//
//	//// Create the rasterizer state from the description we just filled out
//	//device->CreateRasterizerState(&rasterDesc, &rasterState);
//
//	//// Now set the rasterizer state
//	//deviceContext->RSSetState(rasterState);
//
//	// Setup the viewport for rendering
//	D3D11_VIEWPORT viewport;
//	viewport.Width    = (float)screenWidth;
//	viewport.Height   = (float)screenHeight;
//	viewport.MinDepth = 0.0f;
//	viewport.MaxDepth = 1.0f;
//	viewport.TopLeftX = 0.0f;
//	viewport.TopLeftY = 0.0f;
//
//	// Create the viewport
//	deviceContext->RSSetViewports(1, &viewport);
//
//	// Setup the projection matrix
//	float fieldOfView  = (float)3.14159265358979323846 / 4.0f;
//	float screenAspect = (float)screenWidth / (float)screenHeight;
//
//	//// Create the projection matrix for 3D rendering
//	//D3DXMatrixPerspectiveFovLH(&projectionMatrix, fieldOfView, screenAspect, settings.screenNear, settings.screenDepth);
//
//	//// Initialize the world matrix to the identity matrix
//	//D3DXMatrixIdentity(&worldMatrix);
//
//	//// Create an orthographic projection matrix for 2D rendering
//	//D3DXMatrixOrthoLH(&orthoMatrix, (float)screenWidth, (float)screenHeight, settings.screenNear, settings.screenDepth);
//
//
//	// Create states
//	// Clear the blend state description
//	D3D11_BLEND_DESC blendStateDescription;
//	ZERO_MEMORY(blendStateDescription);
//
//	blendStateDescription.RenderTarget[0].BlendEnable = TRUE;
//	blendStateDescription.RenderTarget[0].SrcBlend = D3D11_BLEND_SRC_ALPHA;
//	blendStateDescription.RenderTarget[0].DestBlend = D3D11_BLEND_INV_SRC_ALPHA;
//	blendStateDescription.RenderTarget[0].BlendOp = D3D11_BLEND_OP_ADD;
//	blendStateDescription.RenderTarget[0].SrcBlendAlpha = D3D11_BLEND_ONE;
//	blendStateDescription.RenderTarget[0].DestBlendAlpha = D3D11_BLEND_ONE;
//	blendStateDescription.RenderTarget[0].BlendOpAlpha = D3D11_BLEND_OP_ADD;
//	blendStateDescription.RenderTarget[0].RenderTargetWriteMask = D3D11_COLOR_WRITE_ENABLE_ALL;
//
//	// Create the blend state using the description
//	device->CreateBlendState(&blendStateDescription, &alphaEnableBlendingState);
//
//	// Clear the blend state description
//	ZERO_MEMORY(blendStateDescription);
//
//	blendStateDescription.RenderTarget[0].BlendEnable = FALSE;
//	blendStateDescription.RenderTarget[0].SrcBlend = D3D11_BLEND_SRC_COLOR;
//	blendStateDescription.RenderTarget[0].DestBlend = D3D11_BLEND_DEST_COLOR;
//	blendStateDescription.RenderTarget[0].BlendOp = D3D11_BLEND_OP_ADD;
//	blendStateDescription.RenderTarget[0].SrcBlendAlpha = D3D11_BLEND_SRC_ALPHA;
//	blendStateDescription.RenderTarget[0].DestBlendAlpha = D3D11_BLEND_DEST_ALPHA;
//	blendStateDescription.RenderTarget[0].BlendOpAlpha = D3D11_BLEND_OP_ADD;
//	blendStateDescription.RenderTarget[0].RenderTargetWriteMask = D3D11_COLOR_WRITE_ENABLE_ALL;
//
//	// Create the blend state using the description
//	device->CreateBlendState(&blendStateDescription, &alphaDisableBlendingState);
//
//	
//
//	// Initialize the description of the stencil state
//	D3D11_DEPTH_STENCIL_DESC depthStencilDesc;
//	ZERO_MEMORY(depthStencilDesc);
//
//	depthStencilDesc.DepthEnable = true;
//	depthStencilDesc.DepthWriteMask = D3D11_DEPTH_WRITE_MASK_ALL;
//	depthStencilDesc.DepthFunc = D3D11_COMPARISON_LESS;
//	depthStencilDesc.StencilEnable = true;
//	depthStencilDesc.StencilReadMask = 0xFF;
//	depthStencilDesc.StencilWriteMask = 0xFF;
//
//	// Stencil operations if pixel is front-facing.
//	depthStencilDesc.FrontFace.StencilFailOp = D3D11_STENCIL_OP_KEEP;
//	depthStencilDesc.FrontFace.StencilDepthFailOp = D3D11_STENCIL_OP_INCR;
//	depthStencilDesc.FrontFace.StencilPassOp = D3D11_STENCIL_OP_KEEP;
//	depthStencilDesc.FrontFace.StencilFunc = D3D11_COMPARISON_ALWAYS;
//
//	// Stencil operations if pixel is back-facing
//	depthStencilDesc.BackFace.StencilFailOp = D3D11_STENCIL_OP_KEEP;
//	depthStencilDesc.BackFace.StencilDepthFailOp = D3D11_STENCIL_OP_DECR;
//	depthStencilDesc.BackFace.StencilPassOp = D3D11_STENCIL_OP_KEEP;
//	depthStencilDesc.BackFace.StencilFunc = D3D11_COMPARISON_ALWAYS;
//
//	//// Create the depth stencil state
//	//if (FAILED(device->CreateDepthStencilState(&depthStencilDesc, &depthStencilState)))
//	//	std::cout << "Failed CreateDepthStencilState \n";
//
//	// Clear the second depth stencil state before setting the parameters
//	D3D11_DEPTH_STENCIL_DESC depthDisabledStencilDesc;
//	ZERO_MEMORY(depthDisabledStencilDesc);
//
//	// Now create a second depth stencil state which turns off the Z buffer for 2D rendering.  The only difference is 
//	// that DepthEnable is set to false, all other parameters are the same as the other depth stencil state.
//	depthDisabledStencilDesc.DepthEnable = false;
//	depthDisabledStencilDesc.DepthWriteMask = D3D11_DEPTH_WRITE_MASK_ALL;
//	depthDisabledStencilDesc.DepthFunc = D3D11_COMPARISON_LESS;
//	depthDisabledStencilDesc.StencilEnable = true;
//	depthDisabledStencilDesc.StencilReadMask = 0xFF;
//	depthDisabledStencilDesc.StencilWriteMask = 0xFF;
//	depthDisabledStencilDesc.FrontFace.StencilFailOp = D3D11_STENCIL_OP_KEEP;
//	depthDisabledStencilDesc.FrontFace.StencilDepthFailOp = D3D11_STENCIL_OP_INCR;
//	depthDisabledStencilDesc.FrontFace.StencilPassOp = D3D11_STENCIL_OP_KEEP;
//	depthDisabledStencilDesc.FrontFace.StencilFunc = D3D11_COMPARISON_ALWAYS;
//	depthDisabledStencilDesc.BackFace.StencilFailOp = D3D11_STENCIL_OP_KEEP;
//	depthDisabledStencilDesc.BackFace.StencilDepthFailOp = D3D11_STENCIL_OP_DECR;
//	depthDisabledStencilDesc.BackFace.StencilPassOp = D3D11_STENCIL_OP_KEEP;
//	depthDisabledStencilDesc.BackFace.StencilFunc = D3D11_COMPARISON_ALWAYS;
//
//	//// Create the state using the device
//	//if (FAILED(device->CreateDepthStencilState(&depthDisabledStencilDesc, &depthDisabledStencilState)))
//	//	std::cout << "Failed CreateDepthStencilState \n";
//
//	//disableZBuffer();
//	//enableZBuffer();
//	//enableAlphaBlending();
//	disableAlphaBlending();
//}

void Direct3DDevice::enableAlphaBlending() {
	// Setup the blend factor
	float blendFactor[4];
	blendFactor[0] = 0.0f;
	blendFactor[1] = 0.0f;
	blendFactor[2] = 0.0f;
	blendFactor[3] = 0.0f;

	// Turn on the alpha blending
	deviceContext->OMSetBlendState(alphaEnableBlendingState, blendFactor, 0xffffffff);
}

void Direct3DDevice::disableAlphaBlending() {
	// Setup the blend factor
	float blendFactor[4];
	blendFactor[0] = 0.0f;
	blendFactor[1] = 0.0f;
	blendFactor[2] = 0.0f;
	blendFactor[3] = 0.0f;

	// Turn off the alpha blending
	deviceContext->OMSetBlendState(alphaDisableBlendingState, blendFactor, 0xffffffff);
}

void Direct3DDevice::enableZBuffer() {
	deviceContext->OMSetDepthStencilState(depthStencilState, 1);
}

void Direct3DDevice::disableZBuffer() {
	deviceContext->OMSetDepthStencilState(depthDisabledStencilState, 1);
}

void Direct3DDevice::beginScene() {
	float red = 18.0f/256.0f;
	float green = 84.0f/256.0f;
	float blue = 250.0f/256.0f;
	float alpha = 1.0f;

	float color[4];

	// Setup the color to clear the buffer to
	color[0] = red;
	color[1] = green;
	color[2] = blue;
	color[3] = alpha;

	// Clear the back buffer
	deviceContext->ClearRenderTargetView(renderTargetView, color);

	// Clear the depth buffer
	deviceContext->ClearDepthStencilView(depthStencilView, D3D11_CLEAR_DEPTH, 1.0f, 0);
}

void Direct3DDevice::endScene() {
	// Present the back buffer to the screen since rendering is complete
	swapChain->Present(0, 0);
}