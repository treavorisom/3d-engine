/* -------- PrimitveTypes.h -------- */ #pragma once

#include <limits>

typedef char                f8;
typedef signed char         s8;
typedef unsigned char       u8;

typedef short              f16;
typedef signed  short      s16;
typedef unsigned short     u16;

typedef long               f32;
typedef signed long        s32;
typedef unsigned long      u32;

typedef long long          f64;
typedef signed long long   s64;
typedef unsigned long long u64;



typedef unsigned char       byte;


//typedef float  float32;
//typedef double float64;


//#define  _8_BIT_MAX 0xff
//#define _16_BIT_MAX 0xffff
//#define _32_BIT_MAX 0xffffffff
//#define _64_BIT_MAX 0xffffffffffffffff
//
//#if CHAR_MAX == _8_BIT_MAX
//#endif
//
//#if SCHAR_MAX == _8_BIT_MAX
//#endif
//
//#if UCHAR_MAX == _8_BIT_MAX
//#endif
//
//#if USHRT_MAX == _16_BIT_MAX
//#endif
//
//#if UINT_MAX == _32_BIT_MAX
//#endif
//
//#if ULONG_MAX == _32_BIT_MAX
//#endif
//
//#if ULLONG_MAX == _64_BIT_MAX
//#endif

