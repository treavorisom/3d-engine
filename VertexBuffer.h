/* |-------|    VertexBuffer.h    |-------| */ #pragma once

// Includes and Definitions
#include "Syntax.h"

#include <d3d11.h>

#define Template template <class VertexType>

// Class Declaration
template <class VertexType> class VertexBuffer {

	Public VertexBuffer();
	Public VertexBuffer(ID3D11Device* device, unsigned int size);
	Public VertexBuffer(ID3D11Device* device, int numVertices, VertexType* vertices, int numIndices, unsigned long* indices);

	Public void getPointer(VertexType** bufferPointer);
	Public void release();
	Public void activateBuffer(ID3D11DeviceContext* deviceContext);
	Public void setVertices(ID3D11DeviceContext* deviceContext, VertexType* vertices, int numVertices);

	Public int getNumIndices();
	Public int getNumVertices();

	Private ID3D11Buffer* vertexBuffer;
	Private ID3D11Buffer* indexBuffer;
};

Template VertexBuffer<VertexType>::VertexBuffer(ID3D11Device* device, unsigned int size) {

	// Set up the description of the dynamic vertex buffer.
	D3D11_BUFFER_DESC vertexBufferDesc;
	vertexBufferDesc.Usage = D3D11_USAGE_DYNAMIC;
	vertexBufferDesc.ByteWidth = sizeof(VertexType) * size;
	vertexBufferDesc.BindFlags = D3D11_BIND_VERTEX_BUFFER;
	vertexBufferDesc.CPUAccessFlags = D3D11_CPU_ACCESS_WRITE;
	vertexBufferDesc.MiscFlags = 0;
	vertexBufferDesc.StructureByteStride = 0;

	// Set up the description of the static index buffer.
	D3D11_BUFFER_DESC indexBufferDesc;
	indexBufferDesc.Usage = D3D11_USAGE_DEFAULT;
	indexBufferDesc.ByteWidth = sizeof(unsigned long) * size;
	indexBufferDesc.BindFlags = D3D11_BIND_INDEX_BUFFER;
	indexBufferDesc.CPUAccessFlags = 0;
	indexBufferDesc.MiscFlags = 0;
	indexBufferDesc.StructureByteStride = 0;

	device->CreateBuffer(&vertexBufferDesc, NULL, &vertexBuffer);
	device->CreateBuffer(&indexBufferDesc, NULL, &indexBuffer);
}

Template int VertexBuffer<VertexType>::getNumIndices() {
	D3D11_BUFFER_DESC indexBufferDesc;

	indexBuffer->GetDesc(&indexBufferDesc);

	return indexBufferDesc.ByteWidth / sizeof(unsigned long);
}

Template int VertexBuffer<VertexType>::getNumVertices() {
	D3D11_BUFFER_DESC vertexBufferDesc;

	indexBuffer->GetDesc(&vertexBufferDesc);

	return vertexBufferDesc.ByteWidth / sizeof(VertexType);
}

Template VertexBuffer<VertexType>::VertexBuffer(ID3D11Device* device, int numVertices, VertexType* vertices, int numIndices, unsigned long* indices) {

	// Set up the description of the dynamic vertex buffer.
	D3D11_BUFFER_DESC vertexBufferDesc;
	vertexBufferDesc.Usage               = D3D11_USAGE_DYNAMIC;
	vertexBufferDesc.ByteWidth           = sizeof(VertexType) * numVertices;
	vertexBufferDesc.BindFlags           = D3D11_BIND_VERTEX_BUFFER;
	vertexBufferDesc.CPUAccessFlags      = D3D11_CPU_ACCESS_WRITE;
	vertexBufferDesc.MiscFlags           = 0;
	vertexBufferDesc.StructureByteStride = 0;

	// Give the subresource structure a pointer to the vertex data.
	D3D11_SUBRESOURCE_DATA vertexData;
	vertexData.pSysMem          = vertices;
	vertexData.SysMemPitch      = 0;
	vertexData.SysMemSlicePitch = 0;

	// Set up the description of the static index buffer.
	D3D11_BUFFER_DESC indexBufferDesc;
	indexBufferDesc.Usage               = D3D11_USAGE_DEFAULT;
	indexBufferDesc.ByteWidth           = sizeof(unsigned long) * numIndices;
	indexBufferDesc.BindFlags           = D3D11_BIND_INDEX_BUFFER;
	indexBufferDesc.CPUAccessFlags      = 0;
	indexBufferDesc.MiscFlags           = 0;
	indexBufferDesc.StructureByteStride = 0;

	// Give the subresource structure a pointer to the index data.
	D3D11_SUBRESOURCE_DATA indexData;
	indexData.pSysMem          = indices;
	indexData.SysMemPitch      = 0;
	indexData.SysMemSlicePitch = 0;

	if (FAILED(device->CreateBuffer(&vertexBufferDesc, &vertexData, &vertexBuffer))) throw std::exception("CreateBuffer vertex failed");
	if (FAILED(device->CreateBuffer(&indexBufferDesc, &indexData, &indexBuffer))) throw std::exception("CreateBuffer index failed");
}

Template void VertexBuffer<VertexType>::activateBuffer(ID3D11DeviceContext* deviceContext) {
	// set vertex buffer stride and offset.
	unsigned int stride = sizeof(VertexType);
	unsigned int offset = 0;

	// set the vertex buffer to active in the input assembler so it can be rendered.
	deviceContext->IASetVertexBuffers(0, 1, &vertexBuffer, &stride, &offset);

	// set the index buffer to active in the input assembler so it can be rendered.
	deviceContext->IASetIndexBuffer(indexBuffer, DXGI_FORMAT_R32_UINT, 0);

	// set the type of primitive that should be rendered from this vertex buffer, in this case triangles.
	deviceContext->IASetPrimitiveTopology(D3D11_PRIMITIVE_TOPOLOGY_TRIANGLELIST);
}

Template void VertexBuffer<VertexType>::setVertices(ID3D11DeviceContext* deviceContext, VertexType* vertices, int numVertices) {
	// Lock the vertex buffer so it can be written to.
	D3D11_MAPPED_SUBRESOURCE mappedResource;
	deviceContext->Map(vertexBuffer, 0, D3D11_MAP_WRITE_DISCARD, 0, &mappedResource);

	// Get a pointer to the data in the vertex buffer.
	VertexType* verticesPtr = (VertexType*)mappedResource.pData;

	// Copy the data into the vertex buffer.
	memcpy(verticesPtr, (void*)vertices, (sizeof(VertexType) * numVertices));

	// Unlock the vertex buffer.
	deviceContext->Unmap(vertexBuffer, 0);
}

Template void VertexBuffer<VertexType>::release() {
	RELEASE_R(vertexBuffer);
	RELEASE_R(indexBuffer)
}