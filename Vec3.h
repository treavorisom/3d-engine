/* |-------|    Vec3.h    |-------| */ #pragma once

// Includes and Definitions

#include "Syntax.h"

struct Vec3 {
	Public Vec3() {};
	Public Vec3(float x, float y, float z) { this->x = x; this->y = y; this->z = z; };

	float x, y, z;
};

struct Vec4 {
	Public Vec4() {};
	Public Vec4(float x, float y, float z, float w) { this->x = x; this->y = y; this->z = z; this->w = w; };

	float x, y, z, w;
};

struct Vec2 {
	Public Vec2() {};
	Public Vec2(float x, float y) { this->u = x; this->v = y; };

	union
	{
		struct { float x, y; };
		struct { float u, v; };
	};
};

struct Vertex {
	Vec3 position;
	Vec2 tex;
	Vec3 normal;

	Public Vertex(){}
	Public Vertex(Vec3 position, Vec2 tex, Vec3 normal){ this->position = position; this->tex = tex; this->normal = normal; }
};