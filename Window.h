/* |-------|    Window.h    |-------| */ #pragma once

#include <map>

namespace Windows {

	class Window {

		Private static std::map<HWND, Window*> windows;
		Private static Window* getWindow(HWND hWnd);
		Private static void    setWindow(HWND hWnd, Window* window);
		Private static LRESULT CALLBACK WndProc(HWND hWnd, UINT umessage, WPARAM wparam, LPARAM lparam);

		Private HWND      hWnd;
		Private HINSTANCE hInstance;

		Public void create(int width, int height, int x, int y, char* name);
		
		Private LRESULT CALLBACK messageHandler(HWND hWnd, UINT umessage, WPARAM wparam, LPARAM lparam);

		Public HWND getHwnd() {
			return hWnd;
		}
	};

	std::map<HWND, Window*> Window::windows;

	Window* Window::getWindow(HWND hWnd) {
		try 
		{ 
			return windows.at(hWnd); 
		} 
		catch (std::out_of_range) 
		{ 
			return NULL;
		} 
	}

	void Window::setWindow(HWND hWnd, Window* window) { 
		windows[hWnd] = window; 
	}

	LRESULT CALLBACK Window::WndProc(HWND hWnd, UINT umessage, WPARAM wparam, LPARAM lparam) {

		switch (umessage) {
				// Check if the window is being destroyed
			case WM_DESTROY: {
				PostQuitMessage(0);
				return 0;
			}
				// Check if the window is being closed
			case WM_CLOSE: {
				PostQuitMessage(0);
				return 0;
			}
				// All other messages pass to the message handler in the system class
			default: {
				if (getWindow(hWnd))
					return getWindow(hWnd)->messageHandler(hWnd, umessage, wparam, lparam);

				return DefWindowProc(hWnd, umessage, wparam, lparam);
			}
		}
	}

	LRESULT CALLBACK Window::messageHandler(HWND hWnd, UINT umessage, WPARAM wparam, LPARAM lparam) {
		switch (umessage) {
			case WM_KEYDOWN: {
				// If a key is pressed send it to the input object so it can record that state
				//input->keyDown((unsigned int)wparam);
				return 0;
			}
			case WM_KEYUP: {
				// If a key is released then send it to the input object so it can unset the state for that key
				//input->keyUp((unsigned int)wparam);
				return 0;
			}

			case WM_EXITSIZEMOVE: {
				//graphics->shutdown();
				//graphics->initialize(hWnd);
				return 0;
			}

				// Any other messages send to the default message handler as our application won't make use of them
			default: {

				return DefWindowProc(hWnd, umessage, wparam, lparam);
			}
		}
	}

	void Window::create(int width, int height, int x, int y, char* name) {

		// Get the instance of this application
		hInstance = GetModuleHandle(NULL);

		// Setup the windows class with default settings
		WNDCLASSEX wc;
		wc.style = CS_HREDRAW | CS_VREDRAW | CS_OWNDC;
		wc.lpfnWndProc = WndProc;
		wc.cbClsExtra = 0;
		wc.cbWndExtra = 0;
		wc.hInstance = hInstance;
		wc.hIcon = LoadIcon(NULL, IDI_WINLOGO);
		wc.hIconSm = wc.hIcon;
		wc.hCursor = LoadCursor(NULL, IDC_ARROW);
		wc.hbrBackground = (HBRUSH)GetStockObject(BLACK_BRUSH);
		wc.lpszMenuName = NULL;
		wc.lpszClassName = "CLASS NAME";
		wc.cbSize = sizeof(WNDCLASSEX);

		// Register the window class
		RegisterClassEx(&wc);

		// Determine the resolution of the clients desktop screen
		int screenWidth = getScreenWidth();
		int screenHeight = getScreenHeight();

		//// Setup the screen settings depending on whether it is running in full screen or in windowed mode
		//if (FULL_SCREEN) {
		//	// If full screen set the screen to maximum size of the users desktop and 32bit
		//	DEVMODE dmScreenSettings;
		//	ZERO_MEMORY(dmScreenSettings);

		//	dmScreenSettings.dmSize       = sizeof(dmScreenSettings);
		//	dmScreenSettings.dmPelsWidth  = (unsigned long)screenWidth;
		//	dmScreenSettings.dmPelsHeight = (unsigned long)screenHeight;
		//	dmScreenSettings.dmBitsPerPel = 32;
		//	dmScreenSettings.dmFields     = DM_BITSPERPEL | DM_PELSWIDTH | DM_PELSHEIGHT;

		//	// Change the display settings to full screen
		//	ChangeDisplaySettings(&dmScreenSettings, CDS_FULLSCREEN);
		//}
		//else {
		//	// If windowed then set it to 800x600 resolution
		//	screenWidth = 800;
		//	screenHeight = 600;

		//	// Place the window in the middle of the screen
		//	posX = (GetSystemMetrics(SM_CXSCREEN) - screenWidth) / 2;
		//	posY = (GetSystemMetrics(SM_CYSCREEN) - screenHeight) / 2;
		//}

		// Create the window with the screen settings and get the handle to it
		hWnd = CreateWindowEx(WS_EX_APPWINDOW, "CLASS NAME", name,
			/*WS_CLIPSIBLINGS | WS_CLIPCHILDREN | WS_POPUP*/
			WS_OVERLAPPEDWINDOW,
			x, y, width, height, NULL, NULL, hInstance, NULL);

		setWindow(hWnd, this);

		// Bring the window up on the screen and set it as main focus
		ShowWindow(hWnd, SW_SHOW);
		SetForegroundWindow(hWnd);
		SetFocus(hWnd);

		// Hide the mouse cursor
		ShowCursor(true);
	}

	
}

