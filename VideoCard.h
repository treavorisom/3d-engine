/* |-------|    VideoCard.h    |-------| */ #pragma once

#include <d3d11.h>

struct VideoCard {

	DXGI_ADAPTER_DESC adapterDesc;

	DXGI_MODE_DESC* displayModes;
	unsigned int    numDisplayModes;

	void getAdapterDescription(OUT char* description, OUT unsigned int* length) {
		wcstombs_s(length, description, 128, adapterDesc.Description, 128);
	}

	void initialize() {
		// Create a DirectX graphics interface factory
		IDXGIFactory* factory;
		CreateDXGIFactory(__uuidof(IDXGIFactory), (void**)&factory);

		// Use the factory to create an adapter for the primary graphics interface (video card)
		IDXGIAdapter* adapter;
		factory->EnumAdapters(0, &adapter);

		// Enumerate the primary adapter output (monitor)
		IDXGIOutput* adapterOutput;
		adapter->EnumOutputs(0, &adapterOutput);

		// Get the number of modes that fit the DXGI_FORMAT_R8G8B8A8_UNORM display format for the adapter output (monitor)
		adapterOutput->GetDisplayModeList(DXGI_FORMAT_R8G8B8A8_UNORM, DXGI_ENUM_MODES_INTERLACED, &numDisplayModes, NULL);

		// Create a list to hold all the possible display modes for this monitor/video card combination.
		displayModes = new DXGI_MODE_DESC[numDisplayModes];

		// Now fill the display mode list structures
		adapterOutput->GetDisplayModeList(DXGI_FORMAT_R8G8B8A8_UNORM, DXGI_ENUM_MODES_INTERLACED, &numDisplayModes, displayModes);

		// Now go through all the display modes and find the one that matches the screen width and height
		// When a match is found store the numerator and denominator of the refresh rate for that monitor

		//DXGI_RATIONAL refreshRate;

		//for (int i = 0; i < numModes; i++)
		//	if (displayModeList[i].Width == screenWidth
		//		&& displayModeList[i].Height == screenHeight)
		//		refreshRate = displayModeList[i].RefreshRate;

		// Get the adapter (video card) description
		adapter->GetDesc(&adapterDesc);

		// Store the dedicated video card memory in megabytes
		//videoCardMemory = (int)(adapterDesc.DedicatedVideoMemory / 1024 / 1024);

		//// Convert the name of the video card to a character array and store it
		//unsigned int stringLength;
		//wcstombs_s(&stringLength, videoCardDescription, 128, adapterDesc.Description, 128);

		// Release the display mode list
		//FREE_ARRAY(displayModes);

		// Release the adapter output
		adapterOutput->Release();
		adapterOutput = NULL;

		// Release the adapter
		adapter->Release();
		adapter = NULL;

		// Release the factory
		factory->Release();
		factory = NULL;
	}

	void release() {
		FREE_ARRAY(displayModes);
	}
};