/* |-------|    Windows.cpp    |-------| */ #pragma once

#include "Windows.h"
#include "Log.h"
#include "Graphics.h"
//#include "TextureShader.h"
#include "LightShader.h"


#include "Syntax.h"
#include "Window.h"
#include "Model.h"

#include "Bitmap.h"


#include <DirectXMath.h>

#include <time.h>


int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, PSTR pScmdline, int iCmdshow) {


	//char* testString = "a b c d a aa g";
	//char* delimeters = " ";
	//Tokenizer tokenizer(testString, strlen(testString), delimeters, strlen(delimeters));


	//while (tokenizer.nextEmpty())
	//	std::cout << "(" << tokenizer.tokenToString() << ") " << tokenizer.compareToken("a") << "\n";


	//system("pause");

	Windows::Window window;
	window.create(1600, 900, 0, 0, "My New Window");

	RECT clientRect;
	GetClientRect(window.getHwnd(), &clientRect);

	Direct3DDevice device;
	device.settings.vSyncEnabled = FALSE;
	device.settings.fullscreenEnabled = FALSE;
	device.initialize(window.getHwnd());
	
	LightShader lightShader;
	lightShader.initialize(device.getDevice(), window.getHwnd());

	Model model;
	model.initialize(device.getDevice(), NULL, NULL);


	float rotX = 0, rotY = 0, rotZ = 0;
	float posZ = 5, pozY = 0, posX = 0;


	// Initialize the message structure
	MSG msg;
	ZeroMemory(&msg, sizeof(MSG));

	// Loop until there is a quit message from the window or the user
	while (msg.message != WM_QUIT) {

	
		// Handle the windows messages
		if (PeekMessage(&msg, NULL, 0, 0, PM_REMOVE)) {
			TranslateMessage(&msg);
			DispatchMessage(&msg);
		}

		device.beginScene();

		rotX += 0.0000f;
		rotY += 0.0003f;
		rotZ += 0.0000f;

		ViewMatrix scale = ViewMatrix::scale(1, 1, 1);
		ViewMatrix trans = ViewMatrix::translate(0, 0, 0);
		ViewMatrix scaleTrans = scale * trans;
		ViewMatrix rotation = ViewMatrix::rotateX(rotX) * ViewMatrix::rotateY(rotY) * ViewMatrix::rotateZ(rotZ);
		ViewMatrix modelView = rotation * scaleTrans;

		ViewMatrix viewMatrix = ViewMatrix::rotateY(0) * ViewMatrix::translate(0, 0, 5);
		model.render(device.getDeviceContext());

		Material material;
		material.ka = 0;
		material.kd = 1;
		material.ks = 1;
		material.a = 256;

		lightShader.render(device.getDeviceContext(), model.getVertexBuffer()->getNumIndices(), modelView, viewMatrix, ViewMatrix::getPerspectiveFromFov(0.75, 1600.0f / 900.0f, 1, 1000), material);

		device.endScene();
	}
	
	device.release();
	
	return EXIT_SUCCESS;
}