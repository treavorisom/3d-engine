Texture2D shaderTexture;
SamplerState SampleType;

struct Input
{
	float4 position : SV_POSITION;
	float2 tex : TEXCOORD0;
};

float4 main(Input input) : SV_TARGET0
{
	float4 textureColor;

	// Sample the pixel color from the texture using the sampler at this texture coordinate location.
	textureColor = shaderTexture.Sample(SampleType, input.tex);

	//if (textureColor[3] == 0)
	//	return float4(0, 0, 0, 0);

	//return float4(0, 0, 0, 0);
	//textureColor.r /= textureColor.a;
	//textureColor.g /= textureColor.a;
	//textureColor.b /= textureColor.a;
	//textureColor.a = textureColor.a;
	return textureColor;
}