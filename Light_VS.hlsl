cbuffer MatrixBuffer
{
	float4x4 worldMatrix;
	float4x4  viewMatrix;
	float4x4  projectionMatrix;
};

struct Input
{
	float4 position : POSITION;
	float2 tex      : TEXCOORD0;
	float3 normal   : NORMAL;
};

struct PixelInput
{
	float4 position : SV_POSITION;
	float2 tex      : TEXCOORD0;
	float3 normal   : TEXCOORD1;
	float3 worldPos : TEXCOORD2;
};

PixelInput main(Input input)
{
	PixelInput output = (PixelInput)0;

	// Change the position vector to be 4 units for proper matrix calculations.
	input.position.w = 1.0f;

	// Calculate the position of the vertex against the world, view, and projection matrices.
	input.position = mul(input.position, worldMatrix);

	output.worldPos = input.position;

	output.position = mul(input.position, viewMatrix);
	output.position = mul(output.position, projectionMatrix);

	output.tex = input.tex;

	output.normal = normalize(mul(input.normal, (float3x3)worldMatrix));

	return output;
}