/* |-------|    Windows.h    |-------| */ #pragma once

#include <windows.h>

namespace Windows {

	int getScreenWidth()  { return GetSystemMetrics(SM_CXSCREEN); }
	int getScreenHeight() { return GetSystemMetrics(SM_CYSCREEN); }

}


