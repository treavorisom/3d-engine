Texture2D shaderTexture;
SamplerState SampleType;


struct DirectionalLight
{
	float4 color;
	float3 dir;
};

struct Material
{
	float Ka, Kd, Ks, A;
};

cbuffer PixelShaderBuffer {
	Material material;
	DirectionalLight light;
	
};

struct PixelInput
{
	float4 position : SV_POSITION;
	float2 tex      : TEXCOORD0;
	float3 normal   : TEXCOORD1;
	float3 worldPos : TEXCOORD2;
};

float4 main(PixelInput input) : SV_TARGET0
{
	float3 viewPos = float3(0, 0, -5);

	float4 ambientLight = float4(1, 1, 1, 1);

	DirectionalLight light;
	light.color = float4(1, 1, 1, 1);
	light.dir = normalize(float3(1, -1, 1));

	input.normal = normalize(input.normal);

	float3 V = normalize(viewPos - input.worldPos);
	float3 reflectionDir = reflect(light.dir, input.normal);
	float3 h = normalize(-light.dir + V);

	//float4 textureColor = shaderTexture.Sample(SampleType, input.tex);
	float4 ambientColor = material.Ka * ambientLight;
	float4 diffuseColor = material.Kd * saturate(dot(input.normal, -light.dir));
	float4 specularColor = material.Ks * pow(saturate(dot(input.normal, h)), material.A);

	return ((ambientColor + diffuseColor)  + specularColor) * light.color;
}