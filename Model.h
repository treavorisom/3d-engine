/* |-------|    Model.h    |-------| */ #pragma once

// Includes and Definitions

#include "Syntax.h"
#include "Texture.h"
#include "VertexBuffer.h"
#include "Vec3.h"

#include <vector>


class String {
	Private char* string;

	Public String(char* string, unsigned int length) {

		this->string = new char[length + 1];

		memcpy(this->string, string, length);

		this->string[length] = 0;
	}

	Public ~String() {
		delete[] string;
	}

	Public operator char* () {
		return this->string;
	}
};

struct Tokenizer {
	char* string;
	unsigned int stringLength;

	char* delimeters;
	unsigned int numDelimeters;

	unsigned int offset;
	unsigned int tokenLength;

	Tokenizer(char* string, unsigned int stringLength, char* delimeters, unsigned int numDelimeters) {
		setString(string, stringLength);
		setDelimeters(delimeters, numDelimeters);

		setOffset(0);
		tokenLength = 0;
	}

	void setDelimeters(char* delimeters, unsigned int numDelimeters) {
		this->delimeters = delimeters;
		this->numDelimeters = numDelimeters;
	}

	void setString(char* string, unsigned int stringLength) {
		this->string = string;
		this->stringLength = stringLength;
	}

	void setOffset(unsigned int offset) {
		this->offset = offset;
	}

	int getTokenLength() {
		return offset;
	}

	int compareToken(const char* string) {
		//if (strlen(string) != tokenLength)
		//	return false;

		for (int i = 0; i < tokenLength; i++) 
			if (string[i] != this->string[offset - tokenLength + i]) 
				return false;

		return true;
	}

	int compareDelimeters(const char c) {
		for (int i = 0; i < numDelimeters; i++)
			if (c == delimeters[i])
				return true;

		return false;
	}

	int next() {

		while (compareDelimeters(string[offset]) && offset < stringLength) ++offset;

		tokenLength = offset;

		while (!compareDelimeters(string[offset]) && offset < stringLength) ++offset;

		tokenLength = offset - tokenLength;

		return tokenLength;
	}

	int nextEmpty() {

		if (compareDelimeters(string[offset])) {
			++offset;

			tokenLength = 1;

			return tokenLength;
		}

		tokenLength = offset;

		while (!compareDelimeters(string[offset]) && offset < stringLength) ++offset;

		tokenLength = offset - tokenLength;

		return tokenLength;
	}

	int next(const char delimeter) {
		tokenLength = offset;

		while (string[offset] != delimeter && offset < stringLength)
			++offset;

		tokenLength = offset - tokenLength;

		return tokenLength;
	}

	String tokenToString() {
		return String(string + offset - tokenLength, tokenLength);
	}
};



int importObjectFile(const char* filename, Vertex* &vertices, unsigned int &numVertices, unsigned int* &indices, unsigned int &numIndices) {
	unsigned char* data;
	unsigned int dataSize;

	loadFile(filename, data, dataSize);

	std::vector<Vec3> v_pos;
	std::vector<Vec2> v_tex;
	std::vector<Vec3> v_norm;
	std::vector<Vertex> v_vertices;

	Tokenizer tokenizer((char*)data, dataSize, " \n", 2);

	while (tokenizer.next()) {
		if (tokenizer.compareToken("v")) {

			Vec3 vec3;

			tokenizer.next();
			vec3.x = atof(tokenizer.tokenToString());

			tokenizer.next();
			vec3.y = atof(tokenizer.tokenToString());

			tokenizer.next();
			vec3.z = atof(tokenizer.tokenToString());

			v_pos.push_back(vec3);
		}
		else if (tokenizer.compareToken("vt")) {

			Vec2 vec2;

			tokenizer.next();
			vec2.x = atof(tokenizer.tokenToString());

			tokenizer.next();
			vec2.y = atof(tokenizer.tokenToString());

			v_tex.push_back(vec2);
		}
		else if (tokenizer.compareToken("vn")) {

			Vec3 vec3;

			tokenizer.next();
			vec3.x = atof(tokenizer.tokenToString());

			tokenizer.next();
			vec3.y = atof(tokenizer.tokenToString());

			tokenizer.next();
			vec3.z = atof(tokenizer.tokenToString());

			v_norm.push_back(vec3);
		}
		else if (tokenizer.compareToken("f")) {

			for (int i = 0; i < 3; i++) {
				tokenizer.next();

				String indexString = tokenizer.tokenToString();
				Tokenizer indexTokenizer(indexString, strlen(indexString), "/", 1);

				indexTokenizer.nextEmpty(); // posIndex
				unsigned int posIndex = atoi(indexTokenizer.tokenToString());

				indexTokenizer.nextEmpty(); // /

				indexTokenizer.nextEmpty(); // texIndex
				unsigned int texIndex = atoi(indexTokenizer.tokenToString());


				indexTokenizer.nextEmpty(); // /

				indexTokenizer.nextEmpty(); // normIndex
				unsigned int normIndex = atoi(indexTokenizer.tokenToString());

				
				v_vertices.push_back(Vertex(v_pos[posIndex - 1], v_tex[texIndex - 1], v_norm[normIndex - 1]));
			}
			
		}

		else;
	}




	numVertices = v_vertices.size();
	vertices = new Vertex[numVertices];


	for (int i = 0; i < numVertices; i++)
		vertices[i] = v_vertices[i];

	numIndices = v_vertices.size();
	indices = new unsigned int[numIndices];

	for (unsigned int i = 0; i < numIndices; i++) {
		indices[i] = i;
	}


	return true;
}


// Class Declaration
class Model {

	// Constructors
	Public Model();

	// Methods
	Public void initialize(ID3D11Device* device, char* modelFilename, char* textureFilename);
	Public void render(ID3D11DeviceContext* deviceContext);
	Public void release();

	// Fields
	Private VertexBuffer<Vertex>* vertexBuffer;
	Private Texture* texture;

	Private float scale;
	Private Vec3 position;
	Private Vec3 rotation;

	Public VertexBuffer<Vertex>* getVertexBuffer() { return vertexBuffer; }
};

// Method Definitions
Model::Model() {
	vertexBuffer = NULL;
	//texture = NULL;
	//d3d = NULL;
}

void Model::initialize(ID3D11Device* device, char* modelFilename, char* textureFilename) {

	Vertex* vertices;
	unsigned int numVertices;
	
	unsigned int* indices;
	unsigned int numIndices;


	importObjectFile("monkey.obj", vertices, numVertices, indices, numIndices);

	vertexBuffer = new VertexBuffer<Vertex>(device, (int)numVertices, vertices, (int)numIndices, (unsigned long*)indices);

	delete[] vertices;
	delete[] indices;

	texture = new Texture();
	texture->create("orange_256_256.bmp", device);
}

void Model::render(ID3D11DeviceContext* deviceContext) {
	// Put the vertex and index buffers on the graphics pipeline to prepare them for drawing.
	texture->activate(deviceContext);
	vertexBuffer->activateBuffer(deviceContext);
}

void Model::release() {
	RELEASE(texture);
	RELEASE(vertexBuffer);
}
