/* -------- Files.h -------- */ #pragma once

#include <fstream>

int loadFile(const char* fileName, unsigned char* &data, unsigned int &size) {
	std::ifstream file(fileName, std::ios::in | std::ios::binary | std::ios::ate);

	if (file.is_open()) {

		size = file.tellg();
		data = new unsigned char[size];

		file.seekg(0, std::ios::beg);

		file.read((char*)data, size);

		file.close();
	}
	else
		return false;

	return true;
}