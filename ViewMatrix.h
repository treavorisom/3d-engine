/* |-------|    ViewMatrix.h    |-------| */ #pragma once

#include <math.h>

struct ViewMatrix {

	// Fields
	union {
		float m[4][4];
		struct {
			float m11, m12, m13, m14,
				m21, m22, m23, m24,
				m31, m32, m33, m34,
				m41, m42, m43, m44;
		};
	};

	// Constructors
	ViewMatrix() { ZeroMemory(m, sizeof(m)); }
	ViewMatrix(
		float m11, float m12, float m13, float m14,
		float m21, float m22, float m23, float m24,
		float m31, float m32, float m33, float m34,
		float m41, float m42, float m43, float m44);

	float& operator () (int row, int col)       { return m[row][col]; };
	float  operator () (int row, int col) const { return m[row][col]; };


	ViewMatrix  operator* (ViewMatrix other) {
		return mul(*this, other);
	}

	// Operators
	ViewMatrix operator=(ViewMatrix other) {

		memcpy(this, &other, sizeof(ViewMatrix));

		return *this;
	}

	// Methods
	ViewMatrix transpose() {
		ViewMatrix other = *this;

		m[0][0] = other.m[0][0]; m[0][1] = other.m[1][0]; m[0][2] = other.m[2][0]; m[0][3] = other.m[3][0];
		m[1][0] = other.m[0][1]; m[1][1] = other.m[1][1]; m[1][2] = other.m[2][1]; m[1][3] = other.m[3][1];
		m[2][0] = other.m[0][2]; m[2][1] = other.m[1][2]; m[2][2] = other.m[2][2]; m[2][3] = other.m[3][2];
		m[3][0] = other.m[0][3]; m[3][1] = other.m[1][3]; m[3][2] = other.m[2][3]; m[3][3] = other.m[3][3];

		return *this;
	}


	// Static Methods
	static ViewMatrix getPerspectiveFromFov(float fov, float aspectRatio, float minZ, float maxZ);
	static ViewMatrix getPerspective(float width, float height, float minZ, float maxZ);
	static ViewMatrix rotateX(float angle);
	static ViewMatrix rotateY(float angle);
	static ViewMatrix rotateZ(float angle);
	static ViewMatrix scale(float scale);
	static ViewMatrix scale(float x, float y, float z);
	static ViewMatrix translate(float x, float y, float z);
	static ViewMatrix identity();

	static ViewMatrix mul(ViewMatrix a, ViewMatrix b);
};

// Member definitions
ViewMatrix::ViewMatrix(
	float m11, float m12, float m13, float m14,
	float m21, float m22, float m23, float m24,
	float m31, float m32, float m33, float m34,
	float m41, float m42, float m43, float m44)
{
	m[0][0] = m11; m[0][1] = m12; m[0][2] = m13; m[0][3] = m14;
	m[1][0] = m21; m[1][1] = m22; m[1][2] = m23; m[1][3] = m24;
	m[2][0] = m31; m[2][1] = m32; m[2][2] = m33; m[2][3] = m34;
	m[3][0] = m41; m[3][1] = m42; m[3][2] = m43; m[3][3] = m44;
}


// Static member definitions
ViewMatrix ViewMatrix::getPerspectiveFromFov(float fov, float aspectRatio, float minZ, float maxZ) {

	float yScale = tan(1.57079632679 - fov / 2);
	float xScale = yScale / aspectRatio;

	return ViewMatrix(
		xScale, 0, 0, 0,
		0, yScale, 0, 0,
		0, 0, maxZ / (maxZ - minZ), 1,
		0, 0, -minZ*maxZ / (maxZ - minZ), 0);
}

ViewMatrix ViewMatrix::getPerspective(float width, float height, float minZ, float maxZ) {
	return ViewMatrix(
		2 * minZ / width, 0, 0, 0,
		0, 2 * minZ / height, 0, 0,
		0, 0, maxZ / (maxZ - minZ), 1,
		0, 0, minZ*maxZ / (minZ - maxZ), 0);
}

ViewMatrix ViewMatrix::rotateX(float angle) {
	return ViewMatrix(
		1, 0, 0, 0,
		0, cos(angle), sin(angle), 0,
		0, -sin(angle),  cos(angle), 0,
		0, 0, 0, 1 );
}

ViewMatrix ViewMatrix::rotateY(float angle) {
	return ViewMatrix(
		cos(angle), 0, -sin(angle), 0,
		0, 1, 0, 0,
		sin(angle), 0, cos(angle), 0,
		0, 0, 0, 1);
}

ViewMatrix ViewMatrix::rotateZ(float angle) {
	return ViewMatrix(
		cos(angle), sin(angle), 0, 0,
		-sin(angle), cos(angle), 0, 0,
		0, 0, 1, 0,
		0, 0, 0, 1);
}

ViewMatrix ViewMatrix::identity() {
	return ViewMatrix(
		1, 0, 0, 0,
		0, 1, 0, 0,
		0, 0, 1, 0,
		0, 0, 0, 1);
}

ViewMatrix ViewMatrix::scale(float scale) {
	return ViewMatrix(
		scale, 0, 0, 0,
		0, scale, 0, 0,
		0, 0, scale, 0,
		0, 0, 0, 1);
}

ViewMatrix ViewMatrix::scale(float x, float y, float z) {
	return ViewMatrix(
		x, 0, 0, 0,
		0, y, 0, 0,
		0, 0, z, 0,
		0, 0, 0, 1);
}

ViewMatrix ViewMatrix::translate(float x, float y, float z) {
	return ViewMatrix(
		1, 0, 0, 0,
		0, 1, 0, 0,
		0, 0, 1, 0,
		x, y, z, 1);
}

ViewMatrix ViewMatrix::mul(ViewMatrix a, ViewMatrix b) {
	return ViewMatrix(
		a(0,0)*b(0,0) + a(0,1)*b(1,0) + a(0,2)*b(2,0) + a(0,3)*b(3,0), 
		a(0,0)*b(0,1) + a(0,1)*b(1,1) + a(0,2)*b(2,1) + a(0,3)*b(3,1), 
		a(0,0)*b(0,2) + a(0,1)*b(1,2) + a(0,2)*b(2,2) + a(0,3)*b(3,2), 
		a(0,0)*b(0,3) + a(0,1)*b(1,3) + a(0,2)*b(2,3) + a(0,3)*b(3,3),

		a(1,0)*b(0,0) + a(1,1)*b(1,0) + a(1,2)*b(2,0) + a(1,3)*b(3,0), 
		a(1,0)*b(0,1) + a(1,1)*b(1,1) + a(1,2)*b(2,1) + a(1,3)*b(3,1), 
		a(1,0)*b(0,2) + a(1,1)*b(1,2) + a(1,2)*b(2,2) + a(1,3)*b(3,2), 
		a(1,0)*b(0,3) + a(1,1)*b(1,3) + a(1,2)*b(2,3) + a(1,3)*b(3,3),

		a(2,0)*b(0,0) + a(2,1)*b(1,0) + a(2,2)*b(2,0) + a(2,3)*b(3,0),
		a(2,0)*b(0,1) + a(2,1)*b(1,1) + a(2,2)*b(2,1) + a(2,3)*b(3,1),
		a(2,0)*b(0,2) + a(2,1)*b(1,2) + a(2,2)*b(2,2) + a(2,3)*b(3,2),
		a(2,0)*b(0,3) + a(2,1)*b(1,3) + a(2,2)*b(2,3) + a(2,3)*b(3,3),

		a(3,0)*b(0,0) + a(3,1)*b(1,0) + a(3,2)*b(2,0) + a(3,3)*b(3,0),
		a(3,0)*b(0,1) + a(3,1)*b(1,1) + a(3,2)*b(2,1) + a(3,3)*b(3,1),
		a(3,0)*b(0,2) + a(3,1)*b(1,2) + a(3,2)*b(2,2) + a(3,3)*b(3,2),
		a(3,0)*b(0,3) + a(3,1)*b(1,3) + a(3,2)*b(2,3) + a(3,3)*b(3,3)

		);
}