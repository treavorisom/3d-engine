/* |-------|    Texture.h    |-------| */ #pragma once

#include <d3d11.h>
#include "Log.h"
#include "Syntax.h"
#include "ViewMatrix.h"

#include "Bitmap.h"

#include <exception>


class Texture {

	Private ID3D11Texture2D *pTexture             = NULL;
	Private ID3D11ShaderResourceView* textureView = NULL;

	Public Texture() {}
	Public Texture(const char* filename, ID3D11Device* device) { create(filename, device); }
	//Public ~Texture() { release(); }
	
	Public int create(const char* filename, ID3D11Device* device);
	Public void activate(ID3D11DeviceContext* deviceContext);

	Public void release();
};

int Texture::create(const char* filename, ID3D11Device* device) {

	Bitmap bitmap(filename);

	D3D11_SUBRESOURCE_DATA resourceData;
	resourceData.pSysMem = bitmap.getData();
	resourceData.SysMemPitch = bitmap.getWidth() * 4;
	resourceData.SysMemSlicePitch = NULL;

	D3D11_TEXTURE2D_DESC desc;
	//desc.Width = 8;
	//desc.Height = 8;
	//desc.MipLevels = desc.ArraySize = 1;
	//desc.Format = DXGI_FORMAT_R8G8B8A8_UNORM;
	//desc.SampleDesc.Count = 1;
	//desc.Usage = D3D11_USAGE_DYNAMIC;
	//desc.BindFlags = D3D11_BIND_SHADER_RESOURCE;
	//desc.CPUAccessFlags = D3D11_CPU_ACCESS_WRITE;
	//desc.MiscFlags = 0;

	desc.Width = bitmap.getWidth();
	desc.Height = bitmap.getHeight();
	desc.MipLevels = 1;
	desc.ArraySize = 1;
	desc.Format = DXGI_FORMAT_R8G8B8A8_UNORM;
	desc.SampleDesc.Count = 1;
	desc.SampleDesc.Quality = 0;
	desc.Usage = D3D11_USAGE_DEFAULT;
	desc.BindFlags = D3D11_BIND_SHADER_RESOURCE;
	desc.CPUAccessFlags = 0;
	desc.MiscFlags = 0;

	HRESULT hr = device->CreateTexture2D(&desc, &resourceData, &pTexture);
	if (FAILED(hr))
		return false;


	D3D11_SHADER_RESOURCE_VIEW_DESC SRVDesc;
	memset(&SRVDesc, 0, sizeof(SRVDesc));
	SRVDesc.Format = DXGI_FORMAT_R8G8B8A8_UNORM;
	SRVDesc.ViewDimension = D3D11_SRV_DIMENSION_TEXTURE2D;
	SRVDesc.Texture2D.MipLevels = 1;


	if (FAILED(device->CreateShaderResourceView(pTexture, &SRVDesc, &textureView)))
		return false;
}

void Texture::activate(ID3D11DeviceContext* deviceContext) {
	deviceContext->PSSetShaderResources(0, 1, &textureView);
}

void Texture::release() {
	pTexture->Release();
	textureView->Release();
}