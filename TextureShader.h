/* |-------|    TextureShader.h    |-------| */ #pragma once

#define VS_FILENAME "Texture_VS.cso"
#define PS_FILENAME "Texture_PS.cso"

#include <d3d11.h>
#include "Log.h"
#include "Syntax.h"
#include "ViewMatrix.h"
#include "Files.h"

#include "Bitmap.h"

#include <exception>

// Class Declaration
class TextureShader {
	Private struct MatrixBufferType {
		ViewMatrix world;
		ViewMatrix view;
		ViewMatrix projection;
	};

	Public  TextureShader();
	Public ~TextureShader();

	Public void initialize(ID3D11Device* device, HWND hwnd);
	Public void release();

	Public void render(ID3D11DeviceContext* deviceContext, int indexCount, ViewMatrix worldMatrix, ViewMatrix viewMatrix, ViewMatrix projectionMatrix);

	Private void createShaders(ID3D11Device* device, HWND hwnd, char* vsFileName, char* psFileName);

	Private void setShaderParameters(ID3D11DeviceContext*, ViewMatrix, ViewMatrix, ViewMatrix);
	Private void renderShader(ID3D11DeviceContext*, int);

	Private ID3D11VertexShader* vertexShader;
	Private ID3D11PixelShader*  pixelShader;
	Private ID3D11InputLayout*  layout;
	Private ID3D11Buffer*       matrixBuffer;

	Private ID3D11SamplerState* sampleState;
};

TextureShader::TextureShader() {
	vertexShader = NULL;
	pixelShader = NULL;
	layout = NULL;
	matrixBuffer = NULL;

	sampleState = NULL;
}
TextureShader::~TextureShader() { }


void TextureShader::initialize(ID3D11Device* device, HWND hwnd) {
	createShaders(device, hwnd, VS_FILENAME, PS_FILENAME);
}

void TextureShader::release() {
	RELEASE_R(matrixBuffer);
	RELEASE_R(layout);
	RELEASE_R(pixelShader);
	RELEASE_R(vertexShader);
}

void TextureShader::render(ID3D11DeviceContext* deviceContext, int indexCount, ViewMatrix worldMatrix, ViewMatrix viewMatrix, ViewMatrix projectionMatrix) {
	// Set the shader parameters that it will use for rendering.
	setShaderParameters(deviceContext, worldMatrix, viewMatrix, projectionMatrix);

	// Now render the prepared buffers with the shader.
	renderShader(deviceContext, indexCount);
}

void TextureShader::createShaders(ID3D11Device* device, HWND hwnd, char* vsFileName, char* psFileName) {
	const unsigned int numElements = 2;
	D3D11_INPUT_ELEMENT_DESC polygonLayout[numElements];
	D3D11_BUFFER_DESC matrixBufferDesc;

	// Load and create pixel shader
	unsigned char* data;
	size_t size;

	loadFile(psFileName, data, size);

	if (FAILED(device->CreatePixelShader(data, size, 0, &pixelShader)))
		throw std::exception("Pixel Shader creation failed");

	delete[] data;
	size = 0;

	// Load and create vertex shader
	loadFile(vsFileName, data, size);

	if (FAILED(device->CreateVertexShader(data, size, 0, &vertexShader)))
		throw std::exception("Pixel Shader creation failed");

	//std::cout << "File: " << __FILE__ << " Line: " << __LINE__ << " => Pixel Shader creation failed\n";

	// Create the vertex input layout description.
	// This setup needs to match the VertexType stucture in the ModelClass and in the shader.
	polygonLayout[0].SemanticName = "POSITION";
	polygonLayout[0].SemanticIndex = 0;
	polygonLayout[0].Format = DXGI_FORMAT_R32G32B32_FLOAT;
	polygonLayout[0].InputSlot = 0;
	polygonLayout[0].AlignedByteOffset = 0;
	polygonLayout[0].InputSlotClass = D3D11_INPUT_PER_VERTEX_DATA;
	polygonLayout[0].InstanceDataStepRate = 0;

	polygonLayout[1].SemanticName = "TEXCOORD";
	polygonLayout[1].SemanticIndex = 0;
	polygonLayout[1].Format = DXGI_FORMAT_R32G32_FLOAT;
	polygonLayout[1].InputSlot = 0;
	polygonLayout[1].AlignedByteOffset = D3D11_APPEND_ALIGNED_ELEMENT;
	polygonLayout[1].InputSlotClass = D3D11_INPUT_PER_VERTEX_DATA;
	polygonLayout[1].InstanceDataStepRate = 0;


	// Create the vertex input layout.
	if (FAILED(device->CreateInputLayout(polygonLayout, numElements, data, size, &layout)));

	delete[] data;

	// Setup the description of the dynamic matrix constant buffer that is in the vertex shader.
	matrixBufferDesc.Usage = D3D11_USAGE_DYNAMIC;
	matrixBufferDesc.ByteWidth = sizeof(MatrixBufferType);
	matrixBufferDesc.BindFlags = D3D11_BIND_CONSTANT_BUFFER;
	matrixBufferDesc.CPUAccessFlags = D3D11_CPU_ACCESS_WRITE;
	matrixBufferDesc.MiscFlags = 0;
	matrixBufferDesc.StructureByteStride = 0;

	// Create the constant buffer pointer so we can access the vertex shader constant buffer from within this class.
	if (FAILED(device->CreateBuffer(&matrixBufferDesc, NULL, &matrixBuffer)))
		throw std::exception("CreateBuffer failed");

	// Create a texture sampler state description.
	D3D11_SAMPLER_DESC samplerDesc;

	samplerDesc.Filter = D3D11_FILTER_MIN_MAG_MIP_POINT; // D3D11_FILTER_MIN_MAG_MIP_LINEAR;
	samplerDesc.AddressU = D3D11_TEXTURE_ADDRESS_WRAP;
	samplerDesc.AddressV = D3D11_TEXTURE_ADDRESS_WRAP;
	samplerDesc.AddressW = D3D11_TEXTURE_ADDRESS_WRAP;
	samplerDesc.MipLODBias = 0.0f;
	samplerDesc.MaxAnisotropy = 1;
	samplerDesc.ComparisonFunc = D3D11_COMPARISON_ALWAYS;
	samplerDesc.BorderColor[0] = 0;
	samplerDesc.BorderColor[1] = 0;
	samplerDesc.BorderColor[2] = 0;
	samplerDesc.BorderColor[3] = 0;
	samplerDesc.MinLOD = 0;
	samplerDesc.MaxLOD = D3D11_FLOAT32_MAX;

	// Create the texture sampler state.
	if (FAILED(device->CreateSamplerState(&samplerDesc, &sampleState)))
		std::cout << "CreateSamplerState failed\n";
}

void TextureShader::setShaderParameters(ID3D11DeviceContext* deviceContext, ViewMatrix worldMatrix, ViewMatrix viewMatrix, ViewMatrix projectionMatrix) {
	D3D11_MAPPED_SUBRESOURCE mappedResource;
	MatrixBufferType* dataPtr;
	unsigned int bufferNumber;

	// Transpose the matrices to prepare them for the shader. ????
	worldMatrix.transpose();
	viewMatrix.transpose();
	projectionMatrix.transpose();

	// Lock the constant buffer so it can be written to.
	deviceContext->Map(matrixBuffer, 0, D3D11_MAP_WRITE_DISCARD, 0, &mappedResource);
	//if (FAILED(result));

	// Get a pointer to the data in the constant buffer.
	dataPtr = (MatrixBufferType*)mappedResource.pData;

	// Copy the matrices into the constant buffer.
	dataPtr->world = worldMatrix;
	dataPtr->view = viewMatrix;
	dataPtr->projection = projectionMatrix;

	// Unlock the constant buffer.
	deviceContext->Unmap(matrixBuffer, 0);

	// Set the position of the constant buffer in the vertex shader.
	bufferNumber = 0;

	// Finanly set the constant buffer in the vertex shader with the updated values.
	deviceContext->VSSetConstantBuffers(bufferNumber, 1, &matrixBuffer);
}

void TextureShader::renderShader(ID3D11DeviceContext* deviceContext, int indexCount) {

	// Set the vertex input layout.
	deviceContext->IASetInputLayout(layout);

	// Set the vertex and pixel shaders that will be used to render this triangle.
	deviceContext->VSSetShader(vertexShader, NULL, 0);
	deviceContext->PSSetShader(pixelShader, NULL, 0);

	deviceContext->PSSetSamplers(0, 1, &sampleState);

	// Render the triangle.
	deviceContext->DrawIndexed(indexCount, 0, 0);
}