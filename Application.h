/* |-------|    Application.h    |-------| */ #pragma once

namespace Treavor {

	class Applicaton {

		virtual void start();
		virtual void run();
		virtual void stop();

	};

	class Window {

		void create();
		void destroy();

		void show();
		void hide();


	};


}